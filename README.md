# Enterprise Mail Handler for Jira (JEMH) Downloads #

- https://marketplace.atlassian.com/apps/4832/enterprise-mail-handler-for-jira-jemh

This repository contains deployable JAR files for The Plugin People apps that are normally downloadable through Jira's internal Manage Apps/update.

Updating apps via Jira is the recommended method, however this isn't always possible.  As such we are making release JAR files available here to allow customers to upgrade as needed.

When downloading JAR files (from the Downloads section, see left), be sure they are the right edition (Server vs DataCenter) and that they are _Compatible_ with your Jira.

To upload these JAR's into your JIRA, use: *Jira > Manage Apps > Upload app*. If no versions are listed here we expect that the latest versions are available at the [Atlassian Marketplace](https://marketplace.atlassian.com/apps/4832/enterprise-mail-handler-for-jira-jemh) and your Jira should be able to automatically update to it.

# Jira Data Center Releases #
|Release Date| Filename              | Jira compatibility | Release Summary |
| - | -                  | -               | - | - |
|

# Jira Server Releases #
|Release Date | Filename              | Jira compatibility | Release Summary |
| - | -                  | -               | - |
|